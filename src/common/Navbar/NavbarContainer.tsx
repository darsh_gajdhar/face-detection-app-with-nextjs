import { useRouter } from "next/router";
import Navbar from "./Navbar";

const NavbarContainer = () => {
  const router = useRouter();
  const currentRoute = router.pathname;

  return <Navbar currentRoute={currentRoute} />;
};

export default NavbarContainer;
