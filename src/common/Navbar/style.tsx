import { styled, Box, Typography, Button } from "@mui/material";

export const NavbarComponent = styled(Box, { name: "NavbarComponent" })`
  width: 100%;
  height: 60px;
  background-color: #212121;
  color: white;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0px 50px;
  a {
    text-decoration: none;
    color: white;
    margin-right: 10px;
  }
  @media (max-width: 768px) {
    width: auto;
  }
`;

export const NavTitle = styled(Typography, { name: "NavTitle" })`
  color: white;
`;

export const NavButtonList = styled(Box, { name: "NavButtonList" })``;

export const NavButton = styled(Button, { name: "NavButton" })`
  variant: contained;
  margin: 0px 10px;
  color: white;
  background-color: gray;
`;
