import Link from "next/link";
import { NavbarDetails } from "../../interfaces/interface";
import { NavbarComponent, NavButton, NavButtonList } from "./style";

const Navbar = ({ currentRoute }: NavbarDetails) => (
  <header style={{ position: "sticky", zIndex: "800", top: "0px" }}>
    <NavbarComponent>
      <Link href={"/"}>Face Detection Webcam</Link>
      <NavButtonList>
        <Link href={"/userList"}>
          <NavButton
            variant="contained"
            sx={{
              backgroundColor:
                currentRoute === "/userList" ? "#556cd6" : "gray",
              color: currentRoute === "/userList" ? "white" : "black",
              letterSpacing: "1.3px",
            }}
          >
            Users List
          </NavButton>
        </Link>
        <Link href={"/"}>
          <NavButton
            variant="contained"
            sx={{
              backgroundColor: currentRoute === "/" ? "#556cd6" : "gray",
              color: currentRoute === "/" ? "white" : "black",
              letterSpacing: "1.3px",
            }}
          >
            Webcam
          </NavButton>
        </Link>
      </NavButtonList>
    </NavbarComponent>
  </header>
);

export default Navbar;
