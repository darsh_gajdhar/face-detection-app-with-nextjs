import { Box, Popover, IconButton, styled, Button } from "@mui/material";

export const PopOverBody = styled(Popover, { name: "PopOverBody" })`
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 999;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
`;

export const PopoverChildren = styled(Box, { name: "PopoverChildren" })`
  overflow-y: auto;
  max-height: calc(100vh-250px);
  padding: 1rem;
`;

export const PopoverHeader = styled(Box)`
  display: flex;
  alignitems: center;
  justify-content: space-between;
  margin: 15px;
`;

export const PopoverFooter = styled(Box, { name: "PopoverFooter" })`
  margin: 15px;
  display: flex;
  justify-content: flex-end;
`;

export const CloseButton = styled(IconButton, { name: "CloseButton" })`
  position: absolute;
  right: 8px;
  top: 8px;
  z-index: 3;
  color: gray;
`;

export const SendButton = styled(Button, { name: "SendButton" })`
  height: 35px;
  padding: 6px 16px;
  background-color: #556cd6;
  color: white;
  border-radius: 8px;
  :hover {
    background-color: #556cd6;
  }
  :disabled {
    cursor: not-allowed;
    background-color: rgba(85, 108, 214, 0.3);
  }
`;

export const UserList = styled(Box, { name: "UserList" })`
  display: flex;
  width: 100%;
  text-align: center;
  font-weight: 700;
  padding: 0px 20px;
`;

export const TabList = styled(Box, { name: "TabList" })`
  width: 50%;
  padding: 10px;
  border-radius: 9px;
  cursor: pointer;
`;

export const InputWrapper = styled(Box, { name: "InputWrapper" })`
  width: 100%;
  padding: 14px 10px;
  border-radius: 5px;
  display: flex;
  color: #2121216d;
  border: 1px solid #2121214d;
  :hover {
    border: 1px solid black;
    color: black;
  }
  svg {
    margin-right: 10px;
    cursor: pointer;
  }
  input {
    display: none;
  }
  label {
    cursor: pointer;
  }
`;
