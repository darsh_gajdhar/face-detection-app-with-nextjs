import { useState } from "react";
import { UserPopOverContainerDetails } from "../../interfaces/interface";
import UserPopover from "./UserPopover";

const tabs = [{ title: "Webcam" }, { title: "File Storage" }];

const UserPopoverContainer = ({
  handleSubmit,
  updateFormData,
  formData,
  imageToBlob,
}: UserPopOverContainerDetails) => {
  const [open, setOpen] = useState<boolean>(false);
  const [value, setValue] = useState(0);

  const handleChange = (newValue: number) => {
    setValue(newValue);
  };

  const handleOpen = () => {
    setOpen(true);
    setValue(0);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <UserPopover
      formData={formData}
      imageToBlob={imageToBlob}
      handleSubmit={handleSubmit}
      handleClose={handleClose}
      open={open}
      handleOpen={handleOpen}
      updateFormData={updateFormData}
      value={value}
      tabs={tabs}
      handleChange={handleChange}
    />
  );
};

export default UserPopoverContainer;
