import {
  CloseButton,
  InputWrapper,
  PopOverBody,
  PopoverChildren,
  PopoverFooter,
  PopoverHeader,
  SendButton,
  UserList,
  TabList,
} from "./style";
import CloseIcon from "@mui/icons-material/Close";
import { Divider, Box, Button, TextField, Typography } from "@mui/material";
import { UserPopoverDetails } from "../../interfaces/interface";
import DialogBoxContainer from "../DialogBox/DialogBoxContainer";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import UploadFileSharpIcon from "@mui/icons-material/UploadFileSharp";

const UserPopover = ({
  open,
  handleClose,
  handleOpen,
  updateFormData,
  handleSubmit,
  formData,
  imageToBlob,
  handleChange,
  tabs,
  value,
}: UserPopoverDetails) => (
  <>
    {!open && (
      <Button
        variant="contained"
        onClick={handleOpen}
        style={{ letterSpacing: "2px" }}
      >
        Upload a Picture
      </Button>
    )}

    <PopOverBody
      open={open}
      anchorReference="none"
      transformOrigin={{ vertical: "center", horizontal: "center" }}
      onClose={handleClose}
    >
      <PopoverHeader>
        <Box>
          {value === 0 && <Typography variant="h6">Webcam</Typography>}
          {value === 1 && <Typography variant="h6">File Store</Typography>}
        </Box>
        <CloseButton aria-label="close" onClick={handleClose}>
          <CloseIcon />
        </CloseButton>
      </PopoverHeader>
      <Divider sx={{ borderStyle: "dashed" }} style={{ marginBottom: "5px" }} />
      <PopoverChildren width={700} height={350}>
        <UserList>
          {tabs.map((item, index) => (
            <TabList
              className={value === index ? "active" : "tab"}
              onClick={() => handleChange(index)}
              key={item.title}
              sx={{
                backgroundColor: value === index ? "#556cd6" : "#bdbdbd4d",
                color: value === index ? "white" : "#556cd6",
                letterSpacing: "1.3px",
              }}
            >
              {item.title}
            </TabList>
          ))}
        </UserList>
        <Box padding="20px">
          <form onSubmit={handleSubmit} id="formData">
            {value === 0 && (
              <DialogBoxContainer updateFormData={updateFormData} />
            )}
            {value === 1 && (
              <InputWrapper>
                <UploadFileSharpIcon />
                <label htmlFor="file">Upload Your Image</label>
                <input
                  type="file"
                  id="file"
                  accept="image/*"
                  className="file"
                  name="file"
                  onChange={(event: any) => {
                    imageToBlob(event);
                    toast.success("Image Upload Successfully", {
                      position: toast.POSITION.TOP_RIGHT,
                    });
                  }}
                />
              </InputWrapper>
            )}
            <TextField
              type="text"
              style={{ margin: "20px 0px" }}
              placeholder="Enter your name"
              id="username"
              name="username"
              required
              value={formData.username}
              fullWidth
              onChange={(event: any) =>
                updateFormData({ username: event.target.value })
              }
            />
          </form>
        </Box>
      </PopoverChildren>
      <Divider sx={{ borderStyle: "dashed" }} />
      <PopoverFooter>
        <SendButton
          id="submit"
          disabled={!formData.file || !formData.username}
          value="Upload"
          type="submit"
          onClick={(e: any) => {
            handleSubmit(e);
            handleClose();
          }}
        >
          Upload
        </SendButton>
      </PopoverFooter>
    </PopOverBody>
  </>
);

export default UserPopover;
