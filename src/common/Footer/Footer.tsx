import { FooterWrapper } from "./style";

const Footer = () => (
  <footer>
    <FooterWrapper>Footer</FooterWrapper>
  </footer>
);

export default Footer;
