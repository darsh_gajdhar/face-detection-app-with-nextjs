import { styled, Box } from "@mui/material";

export const FooterWrapper = styled(Box, {
  name: "FooterWrapper",
})`
  width: 100%;
  height: 70px;
  padding: 10px 0px;
  display: flex;
  color: white;
  justify-content: center;
  background-color: #212121;
  align-items: center;
`;
