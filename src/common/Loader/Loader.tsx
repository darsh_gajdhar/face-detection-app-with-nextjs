import { LoaderWrapper } from "./style";
import CircularProgress from "@mui/material/CircularProgress";

interface LoaderDetails {
  loading: any;
  myHeight?: string | number;
}

const Loader = ({ loading }: LoaderDetails) =>
  loading && (
    <LoaderWrapper>
      <CircularProgress />
    </LoaderWrapper>
  );

export default Loader;
