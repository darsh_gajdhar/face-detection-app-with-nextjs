import { styled, Box } from "@mui/material";

export const LoaderWrapper = styled(Box, {
  name: "LoaderWrapper",
  shouldForwardProp: (prop) => prop !== "myHeight" && prop !== "myHeight",
})<{ myHeight?: string | number }>`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 600px;
  grid-area: 1/-1;
  background-color: rgba(0, 0, 0, 0.8);
  z-index: 1;
`;
