import Webcam from "react-webcam";
import { DialogBoxDetails } from "../../interfaces/interface";
import {
  ButtonBox,
  CloseButton,
  DialogModal,
  DialogWrapper,
  ScreenShot,
  ScreenShotWrapper,
} from "./style";
import CloseIcon from "@mui/icons-material/Close";
import CameraIcon from "@mui/icons-material/Camera";
import Loader from "../Loader/Loader";

const DialogBox = ({
  open,
  handleClose,
  handleOpen,
  webcamRef,
  handleSubmit,
  handleUserMedia,
  loading,
}: DialogBoxDetails) => (
  <DialogWrapper>
    <ScreenShotWrapper onClick={handleOpen}>
      <CameraIcon />
      Take Screenshot!
    </ScreenShotWrapper>
    <DialogModal open={open} onClose={handleClose}>
      <CloseButton aria-label="close" onClick={handleClose}>
        <CloseIcon />
      </CloseButton>
      <div
        style={{
          overflow: "hidden",
          height: "400px",
          width: "600px",
          display: "grid",
          gridTemplateColumns: "1fr",
          gridTemplateRows: "1fr",
        }}
      >
        <Loader loading={loading} />
        <Webcam
          ref={webcamRef}
          screenshotFormat="image/jpeg"
          onUserMedia={handleUserMedia}
          style={{ gridArea: "1/-1" }}
        />
      </div>
      <ButtonBox>
        <ScreenShot variant="contained" type="submit" onClick={handleSubmit}>
          Click Me!
        </ScreenShot>
      </ButtonBox>
    </DialogModal>
  </DialogWrapper>
);

export default DialogBox;
