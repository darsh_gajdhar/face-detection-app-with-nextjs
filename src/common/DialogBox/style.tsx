import { styled, Dialog, Box, Button } from "@mui/material";
import IconButton from "@mui/material/IconButton";

export const DialogWrapper = styled(Box, { name: "DialogWrapper" })`
  overflow: hidden;
`;

export const DialogModal = styled(Dialog, { name: "Dialog" })`
  overflow: hidden;
`;

export const CloseButton = styled(IconButton, { name: "CloseButton" })`
  position: absolute;
  right: 8px;
  top: 8px;
  z-index: 3;
  color: gray;
`;

export const ButtonBox = styled(Box, { name: "ButtonBox" })`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 10px 0px;
`;

export const ScreenShot = styled(Button, { name: "ScreenShot" })`
  margin-top: 10px;
  width: 125px;
`;

export const ScreenShotWrapper = styled(Box, { name: "ScreenShotWrapper" })`
  width: 100%;
  padding: 14px 10px;
  border-radius: 5px;
  display: flex;
  color: #2121216d;
  border: 1px solid #2121214d;
  :hover {
    border: 1px solid black;
    color: black;
  }
  svg {
    margin-right: 10px;
    cursor: pointer;
  }
`;
