import { useCallback, useRef, useState } from "react";
import { DialogContainerDetails } from "../../interfaces/interface";
import DialogBox from "./DialogBox";
import Webcam from "react-webcam";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const DialogBoxContainer = ({ updateFormData }: DialogContainerDetails) => {
  const webcamRef = useRef<Webcam>(null);
  const [open, setOpen] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(true);

  const handleUserMedia = () => setTimeout(() => setLoading(false), 2000);

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmit = useCallback(
    (event: any) => {
      event.preventDefault();
      const pictureSrc = webcamRef.current?.getScreenshot();
      updateFormData({ file: pictureSrc });
      toast.success("Screenshot Capture", {
        position: toast.POSITION.TOP_RIGHT,
      });
      handleClose();
    },
    [updateFormData]
  );

  return (
    <DialogBox
      open={open}
      handleOpen={handleOpen}
      handleClose={handleClose}
      webcamRef={webcamRef}
      handleSubmit={handleSubmit}
      handleUserMedia={handleUserMedia}
      loading={loading}
    />
  );
};

export default DialogBoxContainer;
