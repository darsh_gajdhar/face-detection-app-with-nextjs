import { LayoutDetails } from "../../interfaces/interface";
import Layout from "./Layout";

const LayoutContainer = ({ children }: LayoutDetails) => (
  <Layout>{children}</Layout>
);

export default LayoutContainer;
