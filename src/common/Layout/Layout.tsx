import Head from "next/head";
import { LayoutDetails } from "../../interfaces/interface";
import Footer from "../Footer/Footer";
import NavbarContainer from "../Navbar/NavbarContainer";

const Layout = ({ children }: LayoutDetails) => (
  <>
    <Head>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <NavbarContainer />
    <main style={{ height: "calc(100vh - 130px)", overflowY: "auto" }}>
      {children}
    </main>
    <Footer />
  </>
);

export default Layout;
