import { Box, styled, Typography } from "@mui/material";

export const UserCardWrapper = styled(Box, { name: "UserCardWrapper" })`
  box-shadow: 0 2px 8px rgba(0, 0, 0, 0.1);
  margin-top: 10px;
  height: auto;
  overflow: hidden;
  border-radius: 5px;
`;

export const UserCardImage = styled(Box, { name: "UserCardImage" })`
display:flex;
justify-content:center;
border-radius: 5px;
img: {    
    object-fit:cover;

  },
`;

export const UserCardInfoName = styled(Typography, {
  name: "UserCardInfoName",
})`
  font-size: 20px;
  text-align: center;
`;
