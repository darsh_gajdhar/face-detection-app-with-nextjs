import Image from "next/image";
import { UserCardImage, UserCardInfoName, UserCardWrapper } from "./style";

export interface UserCardDetails {
  image: string;
  name: string;
}

const UserCard = ({ image, name }: UserCardDetails) => (
  <UserCardWrapper>
    <UserCardImage>
      <Image
        alt={name}
        src={image}
        width="350"
        height="350"
        crossOrigin="anonymous"
        style={{ objectFit: "contain" }}
      />
    </UserCardImage>
    <UserCardInfoName>{name}</UserCardInfoName>
  </UserCardWrapper>
);

export default UserCard;
