import { Button, Grid } from "@mui/material";
import React from "react";
import UserCard from "../../common/UserCard/UserCard";
import { UserListDetails } from "../../interfaces/interface";
import UploadFileIcon from "@mui/icons-material/UploadFile";
import FormDataContainer from "../FormData/FormDataContainer";
import {
  EmptyData,
  EmptyDataWrapper,
  UserListForm,
  UserListWrapper,
} from "./style";

const UserList = ({ users, isData }: UserListDetails) => (
  <UserListWrapper>
    <UserListForm>
      <FormDataContainer />
    </UserListForm>
    {!isData ? (
      <EmptyDataWrapper>
        <UploadFileIcon />
        <EmptyData>You have not Uploaded any Data!</EmptyData>
      </EmptyDataWrapper>
    ) : (
      <Grid container spacing={4} marginTop={10}>
        {users?.map((user, index) => {
          const image = user.filepath as string;
          return (
            <Grid item key={index} xl={3} lg={2} xs={12}>
              <UserCard image={image} name={user.name} />
            </Grid>
          );
        })}
      </Grid>
    )}
  </UserListWrapper>
);

export default UserList;
