import { Box, styled, Typography } from "@mui/material";

export const UserListWrapper = styled(Box, { name: "UserListWrapper" })`
  label {
    color: blue;
  }

  padding: 0px 50px;
`;

export const UserListForm = styled(Box, { name: "UserListForm" })`
  display: flex;
  padding-top: 20px;
  align-items: flex-start;
`;

export const EmptyDataWrapper = styled(Box, { name: "EmtpyDataWrapper" })`
  display: flex;
  justify-content: center;
  align-items: center;
  color: rgba(0, 0, 0, 0.2);
  margin: 80px 0px;
  height: 300px;
`;

export const EmptyData = styled(Typography, { name: "EmptyData" })`
  font-size: 24px;
  color: rgba(0, 0, 0, 0.2);
`;
