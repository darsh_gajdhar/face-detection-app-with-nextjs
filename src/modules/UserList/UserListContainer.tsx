import React, { useEffect, useState } from "react";
import { useAppSelector } from "../../store/configureStore";
import UserList from "./UserList";

const UserListContainer = () => {
  const [isData, setIsData] = useState<boolean>(false);

  const users = useAppSelector((state) => state.user.users);

  useEffect(() => {
    if (users.length > 0) {
      setIsData(true);
    }
  }, [users]);

  return <UserList users={users} isData={isData} />;
};

export default UserListContainer;
