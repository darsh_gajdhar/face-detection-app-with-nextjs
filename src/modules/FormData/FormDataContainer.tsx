import React, { useEffect, useReducer } from "react";
import { useAppDispatch } from "../../store/configureStore";
import { userData } from "../../store/features/userSlice";
import FormData from "./FormData";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const FormDataContainer = () => {
  const dispatch = useAppDispatch();
  const [formData, updateFormData] = useReducer(
    (currentState: any, newState: any) => ({ ...currentState, ...newState }),
    {
      username: "",
      file: "",
    }
  );

  const imageToBlob = (event: any) => {
    const image = event.target.files[0];
    if (image) {
      const fileReader = new FileReader();
      fileReader.addEventListener("load", function (evt) {
        updateFormData({ file: evt.target?.result });
      });
      fileReader.readAsDataURL(image);
    }
  };

  const handleSubmit = (event: any) => {
    event.preventDefault();

    dispatch(
      userData({
        name: formData.username,
        filepath: formData.file,
      })
    );
    toast.success("Data uploaded successfully", {
      position: toast.POSITION.TOP_RIGHT,
    });
    updateFormData({ file: "", username: "" });
  };

  return (
    <FormData
      handleSubmit={handleSubmit}
      formData={formData}
      updateFormData={updateFormData}
      imageToBlob={imageToBlob}
    />
  );
};
export default FormDataContainer;
