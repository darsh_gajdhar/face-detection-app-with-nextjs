import { Box, styled, Button } from "@mui/material";

export const ImageUploadContents = styled(Box, { name: "ImageUploadContents" })`
  width: 100%;
  display: flex;
  justify-content: center;
  margin: 20px 0px;
  align-items: center;
  form {
    width: 100%;
    display: flex;
    justify-content: center;
  }
  input.file {
    display: none;
  }
  input {
    margin: 0px 10px;
  }
  input[disabled] {
    cursor: not-allowed;
    color: rgba(f, f, f, 0.3);
    background-color: rgba(85, 108, 214, 0.3);
  }
  label {
    color: blue;
    display: flex;
    cursor: pointer;
  }
`;

export const SendButton = styled(Button, { name: "SendButton" })`
  height: 35px;
  padding: 6px 16px;
  background-color: #556cd6;
  color: white;
  border-radius: 8px;
  :hover {
    background-color: #556cd6;
  }
  :disabled {
    cursor: not-allowed;
    background-color: rgba(85, 108, 214, 0.3);
  }
`;

export const FormContents = styled(Box, { name: "FormContents" })`
  display: flex;
  justify-content: center;
  width: 100%;
`;

export const InputWrapper = styled(Box, { name: "InputWrapper" })`
  width: 100%;
  padding: 14px 10px;
  border-radius: 5px;
  display: flex;
  color: #2121216d;
  border: 1px solid #2121214d;
  :hover {
    border: 1px solid black;
    color: black;
  }
  svg {
    margin-right: 10px;
    cursor: pointer;
  }
  input {
    display: none;
  }
  label {
    cursor: pointer;
  }
`;
