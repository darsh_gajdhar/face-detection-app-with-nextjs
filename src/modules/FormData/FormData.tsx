import React from "react";
import UploadFileSharpIcon from "@mui/icons-material/UploadFileSharp";
import { FormContents, ImageUploadContents, SendButton } from "./style";
import { Button } from "@mui/material";
import { FormDetails } from "../../interfaces/interface";
import DialogBoxContainer from "../../common/DialogBox/DialogBoxContainer";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import UserPopoverContainer from "../../common/UserPopover/UserPopoverContainer";

const FormData = ({
  handleSubmit,
  formData,
  updateFormData,
  imageToBlob,
}: FormDetails) => (
  <>
    <FormContents>
      <UserPopoverContainer
        updateFormData={updateFormData}
        handleSubmit={handleSubmit}
        formData={formData}
        imageToBlob={imageToBlob}
      />
    </FormContents>
  </>
);

export default FormData;
