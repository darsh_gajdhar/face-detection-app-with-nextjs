import React, { useRef, useEffect, useState, useCallback } from "react";
import WebcamDetection from "./WebcamDetection";
import * as faceapi from "face-api.js";
import Webcam from "react-webcam";
import { TNetInput } from "face-api.js/build/commonjs/dom";
import { useAppSelector } from "../../store/configureStore";
import { LabeledFaceDescriptors, WithFaceDescriptor } from "face-api.js";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

let faceInterval: any;

const videoConstraints = {
  width: "100%",
  height: "100%",
};

const WebcamDetectionContainer = () => {
  const webCamRef = useRef<Webcam>(null);
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const [isLoaded, setIsLoaded] = useState<boolean>(false);
  const users = useAppSelector((state) => state.user.users);

  const loadModels = async () => {
    const MODEL_URL = "/models/";
    await Promise.all([
      faceapi.nets.tinyFaceDetector.load(MODEL_URL),
      faceapi.nets.faceExpressionNet.load(MODEL_URL),
      faceapi.nets.faceLandmark68Net.load(MODEL_URL),
      faceapi.nets.faceRecognitionNet.load(MODEL_URL),
    ]);
  };

  const loadTestImage = useCallback(async () => {
    await loadModels();
    return Promise.all(
      users?.map(async (user) => {
        const image = await faceapi?.fetchImage(user.filepath as string);
        const detections = await faceapi
          ?.detectSingleFace(image, new faceapi.TinyFaceDetectorOptions())
          .withFaceLandmarks()
          .withFaceDescriptor();
        let descriptions = [] as
          | Float32Array[]
          | LabeledFaceDescriptors
          | WithFaceDescriptor<any>
          | Float32Array
          | Array<
              LabeledFaceDescriptors | WithFaceDescriptor<any> | Float32Array
            >;
        descriptions?.push(detections?.descriptor);
        return new faceapi.LabeledFaceDescriptors(user.name, descriptions);
      })
    );
  }, [users]);

  const faceDetect = useCallback(async () => {
    try {
      setIsLoaded(true);
      await loadModels();
      if (webCamRef.current && canvasRef.current) {
        const webcam = webCamRef.current.video as HTMLVideoElement;
        const canvas = canvasRef.current;
        if (webcam.width <= 0 && webcam.height <= 0) return;

        faceapi.matchDimensions(canvas, {
          width: webcam.width,
          height: webcam.height,
        });
        const video = webCamRef.current.video;
        const labeledFaceDescriptors = await loadTestImage();
        if (labeledFaceDescriptors?.length > 0) {
          const faceMatcher = new faceapi.FaceMatcher(
            labeledFaceDescriptors,
            0.5
          );
          {
            toast.success("Please Smile while facing Camera :)", {
              position: toast.POSITION.TOP_RIGHT,
              toastId: 1,
            });
          }
          setIsLoaded(false);
          faceInterval = setInterval(async () => {
            try {
              const width = webcam.width || 1;
              const height = webcam.height || 1;
              const detections = await faceapi
                .detectAllFaces(
                  video as TNetInput,
                  new faceapi.TinyFaceDetectorOptions()
                )
                .withFaceLandmarks()
                .withFaceDescriptors();
              const resizedDetections = faceapi?.resizeResults(detections, {
                width: width,
                height: height,
              });

              let ctx = canvas?.getContext("2d");

              ctx?.clearRect(0, 0, width, height);

              const results = resizedDetections?.map((d) =>
                faceMatcher?.findBestMatch(d.descriptor)
              );

              results.forEach((result, i) => {
                if (results?.length > 0) {
                  const box = resizedDetections[i].detection.box;
                  const drawBox = new faceapi.draw.DrawBox(box, {
                    label: result.label,
                  });
                  drawBox.draw(canvas);
                }
              });
            } catch (error) {
              console.log(">>> Error:", error);
              toast.error("Error Happened Please Refresh Page", {
                position: toast.POSITION.TOP_RIGHT,
              });
            }
          }, 200);
        } else {
          setIsLoaded(false);
          toast.error("You have not Entered the Details", {
            position: toast.POSITION.TOP_RIGHT,
          });
        }
      }
    } catch (error) {
      setIsLoaded(false);
      toast.error("Error Happened Please Refresh Page", {
        position: toast.POSITION.TOP_RIGHT,
      });
    }
  }, [loadTestImage]);

  useEffect(() => {
    if (users.length > 0) {
      faceDetect();
    }

    return () => {
      clearInterval(faceInterval);
      toast.dismiss(1);
    };
  }, [faceDetect, users]);

  return (
    <WebcamDetection
      webCamRef={webCamRef}
      canvasRef={canvasRef}
      faceDetect={faceDetect}
      isLoaded={isLoaded}
      setIsLoaded={setIsLoaded}
      videoConstraints={videoConstraints}
    />
  );
};

export default WebcamDetectionContainer;
