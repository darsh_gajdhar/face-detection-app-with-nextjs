import React from "react";
import Webcam from "react-webcam";
import Loader from "../../common/Loader/Loader";
import { WebcamDetails } from "../../interfaces/interface";
import { CanvasImage, VideoWrapper, VideoBox, WebcamWrapper } from "./style";

const WebcamDetection = ({ webCamRef, canvasRef, isLoaded }: WebcamDetails) => (
  <WebcamWrapper>
    <VideoWrapper>
      <VideoBox>
        <Loader loading={isLoaded} />
        <Webcam
          crossOrigin="anonymous"
          ref={webCamRef}
          width={600}
          height={500}
          videoConstraints
          allowFullScreen={true}
          style={{
            gridArea: " 1 / -1 ",
          }}
        />
        <CanvasImage ref={canvasRef} width={600} height={500} />
      </VideoBox>
    </VideoWrapper>
  </WebcamWrapper>
);

export default WebcamDetection;
