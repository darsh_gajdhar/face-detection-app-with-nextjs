import { styled, Box, Button } from "@mui/material";

export const WebcamWrapper = styled(Box, { name: "WebcamWrapper" })`
  display: flex;
`;

export const VideoWrapper = styled(Box, { name: "VideoWrapper" })`
  margin-top: 14px;
  border-radius: 4px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const VideoBox = styled(Box, { name: "VideoBox" })`
  object-fit: cover;
  outline: none;
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: 1fr;
`;

export const CanvasImage = styled("canvas", { name: "CanvasImage" })`
  grid-area: 1/-1;
  z-index: 2;
`;

export const DetailButton = styled(Button, { name: "DetailButton" })`
  height: 35px;
  width: 140px;
`;
