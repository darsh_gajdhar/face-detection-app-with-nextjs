import { styled, Box, Typography } from "@mui/material";

export const HomeComponent = styled(Box, { name: "HomeComponent" })``;

export const Title = styled(Typography, { name: "HomeTitle" })`
  color: white;
`;

export const HomeWrapper = styled(Box, {
  name: "HomeWrapper",
})`
  display: flex;
  justify-content: center;
`;
