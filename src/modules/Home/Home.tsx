import WebcamDetectionContainer from "../WebcamFaceDetection/WebcamDetectionContainer";
import { HomeComponent, HomeWrapper } from "./style";

const Home = () => (
  <HomeComponent>
    <HomeWrapper>
      <WebcamDetectionContainer />
    </HomeWrapper>
  </HomeComponent>
);

export default Home;
