import { AnyAction, Dispatch, ThunkDispatch } from "@reduxjs/toolkit";
import { ReactNode, SetStateAction } from "react";
import Webcam from "react-webcam";
import { User } from "../store/features/userSlice";

export interface LayoutDetails {
  children: ReactNode;
}

export interface WebcamDetails {
  webCamRef: React.RefObject<Webcam>;
  canvasRef: React.RefObject<HTMLCanvasElement>;
  faceDetect: () => Promise<void>;
  isLoaded: boolean;
  setIsLoaded: React.Dispatch<React.SetStateAction<boolean>>;
  videoConstraints: { width: string; height: string };
}

export interface FormDetails {
  handleSubmit: (event: React.FormEvent<HTMLFormElement>) => void;
  formData: any;
  updateFormData: React.Dispatch<any>;
  imageToBlob: (event: any) => void;
}

export interface InputValue {
  file: string;
  username: string;
}

export interface UserListDetails {
  users: User[];
  isData: boolean;
}

export interface DialogBoxDetails {
  open: boolean;
  handleOpen: () => void;
  handleClose: () => void;
  webcamRef?: React.RefObject<Webcam>;
  handleSubmit: (event: any) => void;
  handleUserMedia: () => NodeJS.Timeout;
  loading: boolean;
}

export interface DialogContainerDetails {
  updateFormData: React.Dispatch<any>;
  // function
}

export interface UserPopOverContainerDetails {
  updateFormData: React.Dispatch<any>;
  handleSubmit: (event: React.FormEvent<HTMLFormElement>) => void;
  formData: any;
  imageToBlob: (event: any) => void;
}

export interface UserPopoverDetails {
  open: boolean;
  handleClose: () => void;
  handleOpen: () => void;
  updateFormData: React.Dispatch<any>;
  handleSubmit: (event: React.FormEvent<HTMLFormElement>) => void;
  formData: any;
  imageToBlob: (event: any) => void;
  value: number;
  tabs: {
    title: string;
  }[];
  handleChange: (newValue: number) => void;
}

export interface NavbarDetails {
  currentRoute: string;
}
