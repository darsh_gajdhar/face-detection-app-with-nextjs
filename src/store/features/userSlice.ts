import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface User {
  name: string;
  filepath: string | ArrayBuffer | null | undefined;
}

interface UserState {
  users: User[];
}

const initialState: UserState = {
  users: [],
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    userData: (
      state,
      action: PayloadAction<{
        name: string;
        filepath: string | ArrayBuffer | null | undefined;
      }>
    ) => {
      state.users.push({
        name: action.payload.name,
        filepath: action.payload.filepath,
      });
    },
  },
});

export default userSlice.reducer;
export const { userData } = userSlice.actions;
