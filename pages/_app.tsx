import { CacheProvider, EmotionCache } from "@emotion/react";
import { CssBaseline } from "@mui/material";
import { ThemeProvider } from "@mui/material/styles";
import type { AppProps } from "next/app";
import Head from "next/head";
import { Provider } from "react-redux";
import { ToastContainer } from "react-toastify";
import LayoutContainer from "../src/common/Layout/LayoutContainer";
import createEmotionCache from "../src/mui_imports/createEmotionCache";
import theme from "../src/mui_imports/theme";
import { store } from "../src/store/configureStore";
import ErrorBoundary from "./error";

interface MyAppProps extends AppProps {
  emotionCache?: EmotionCache;
}

const clientSideEmotionCache = createEmotionCache();

const MyApp = (props: MyAppProps) => {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;

  return (
    <CacheProvider value={emotionCache}>
      <Head>
        <title>Main page</title>
        <meta name="viewport" content="initial-scale=1,width=device-width" />
      </Head>
      <ErrorBoundary>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <Provider store={store}>
            <LayoutContainer>
              <Component {...pageProps} />
              <ToastContainer />
            </LayoutContainer>
          </Provider>
        </ThemeProvider>
      </ErrorBoundary>
    </CacheProvider>
  );
};

export default MyApp;
